#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
    rlPhaseStartSetup
        rlRun "set -o pipefail"
        test -n "$REPOSITORY" || rlDie "Required variable REPOSITORY not defined"
    rlPhaseEnd

    rlPhaseStartTest "Test installation of RPMS built by Packit"
        # RHEL 6 does not ship copr plugin, install it from a copr repository
        if rlIsRHEL "=6" || rlIsCentOS "=6"; then
            rlRun "rpm -ivh RHEL6_YUM_COPR_PLUGIN"
        fi

        rpm -q fedora-repos-rawhide &>/dev/null && CHROOT="fedora-rawhide-$(arch)"

        # From RHEL8+, CentOS8+ and Fedora we use `dnf`
        if rlIsRHEL ">7" || rlIsCentOS ">7" || rlIsFedora; then
           rlRun "dnf -y copr enable $REPOSITORY $CHROOT" || rlDie "Failed to enable repository $REPOSITORY, cannot continue"
           reponame="$(dnf repolist | grep copr: | awk '{print $1}' | head -1)"
           # FIXME: Revert filtering out the verbose dnf output once https://github.com/rpm-software-management/dnf5/issues/570 is fixed
           nvrs="$(dnf -q repoquery --latest-limit 1 --disablerepo=* --enablerepo=$reponame | grep -v -E 'Copr repo' | grep  -v -E \.src$ | xargs)"
           rlRun "dnf -y install --allowerasing $nvrs" || rlDie "Failed to install al packages in the repository"

        # For older RHEL clones we need to use `yum`
        elif rlIsRHEL "<8" || rlIsCentOS "<8"; then
           rlRun "yum -y copr enable $REPOSITORY" || rlDie "Failed to enable $REPOSITORY, cannot continue"
           rlRun "reponame=$(yum repolist | grep copr: | awk '{print $1}' | sed "s|/$(arch)||")"
           rlRun "nvrs=$(repoquery --disablerepo=* --enablerepo=$reponame | grep -v -E '\.src$' | xargs)"
           rlRun "yum -y install $nvrs" || rlDie "Failed to install al packages in the repository"

        # Distro is unsupported, print a nice error
        else
            test -e /etc/os-release && source /etc/os-release
            rlDie "Unsupported distribution $PRETTY_NAME"
        fi

        # Verify all packages successfully installed
        rlRun "rpm -q $nvrs" 0 "Verify all packages can be installed"
    rlPhaseEnd
rlJournalEnd
